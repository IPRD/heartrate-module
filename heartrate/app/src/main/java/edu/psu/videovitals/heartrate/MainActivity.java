package edu.psu.videovitals.heartrate;

import android.content.Intent;
import android.icu.lang.UCharacter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import org.json.*;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    final int HEART_RATE = 1;
    final int NO_CONDITION = 0;
    int healthCondition = NO_CONDITION;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = getIntent();
        String servicerequest = intent.getStringExtra("serviceRequest");
        if(servicerequest != null) Log.d("Svc req",servicerequest);
        try {
            JSONObject svcJSON = new JSONObject(servicerequest);
            String svcLOINCCode = svcJSON.getJSONObject("code").getJSONArray("coding").getJSONObject(0).getString("code");
            Log.d("ConditionCode",svcLOINCCode);
            if (svcLOINCCode.equals("8867-4"))
                healthCondition = HEART_RATE;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Button s = (Button) findViewById(R.id.submitb);
        s.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    @Override
    public void finish() {
        // Prepare data intent
        EditText val = (EditText) findViewById(R.id.bpm);
        Intent data = new Intent();
        String resOut = "{ \"code\": { \"coding\": [{ \"system\": \"http://loinc.org\", \"code\": \"8867-4\", \"display\": \"Heart rate\" }], \"text\": \"Heart rate\" }, \"effectiveDateTime\": \"1999-07-02\", \"meta\": {\"profile\": [\"http://hl7.org/fhir/StructureDefinition/vitalsigns\"]}, \"subject\": {\"reference\": \"Patient/example\"}, \"id\": \"heart-rate\", \"category\": [{ \"coding\": [{ \"system\": \"http://terminology.hl7.org/CodeSystem/observation-category\", \"code\": \"vital-signs\", \"display\": \"Vital Signs\" }], \"text\": \"Vital Signs\" }], \"resourceType\": \"Observation\", \"status\": \"final\", \"valueQuantity\": { \"unit\": \"beats/minute\", \"system\": \"http://unitsofmeasure.org\", \"code\": \"/min\", \"value\": -1 } }";
        try {
            JSONObject resJSON = new JSONObject(resOut);
            String date = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm:ss.SSSZ").format(new Date());
            resJSON.put("effectiveDateTime",date);
            if(healthCondition == HEART_RATE) {
                resJSON.getJSONObject("valueQuantity").put("value", val.getText().toString());
            }
            data.putExtra("observationResponse",resJSON.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            data.putExtra("observationResponse",resOut);
        }
        // Activity finished ok, return the data
        setResult(RESULT_OK, data);
        super.finish();
    }

}
